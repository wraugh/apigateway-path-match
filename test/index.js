/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

const tap = require('tap')
const PathMatcher = require('../src/index.js');

/**
 * Add all given patterns to an instance of PathMatcher,
 * then test that the test lookups match the expected pattern.
 *
 * A test case looks like this:
 *
 *      [method-pattern, path-pattern, [test-method, test-path, ...]]
 *
 * The first two elements are the arguments passed to `PathMatcher.set`.
 * The third argument is a list of (method, path) pairs to lookup against the
 * path matcher. It's expected that these lookups match the pattern defined by
 * the first two elements.
 *
 * In addition to the cases that must match, using the optional second argument
 * you can list [method, path] pairs which should fail to match.
 */
function testSet(cases, nomatch) {
    const pm = new PathMatcher();
    for (let phase of ["set", "match"]) {
        for (let c of cases) {
            let id = JSON.stringify(c);
            switch (phase) {
                case "set":
                    pm.set(c[0], c[1], id);
                    break;
                case "match":
                    while (c[2].length > 0) {
                        let method = c[2].shift();
                        let path = c[2].shift();
                        tap.equal(pm.match(method, path), id, method + " " + path);
                    }
                    break;
            }
        }
    }

    if (nomatch) {
        for (let c of nomatch) {
            tap.equal(pm.match(c[0], c[1]), null, c[0] + " " + c[1]);
        }
    }
}

// We can query an empty path matcher, but it won't match anything.
const emptyPm = new PathMatcher();
tap.notOk(emptyPm.match("GET", "/"));

// Typical webapp
testSet([
    ["GET",    "/foo",            ["GET",    "/foo"]],
    ["GET",    "/foo/bar",        ["GET",    "/foo/bar"]],
    ["GET",    "/foo/{id}",       ["GET",    "/foo/123",  "GET",    "/foo/abc"]],
    ["POST",   "/foo",            ["POST",   "/foo"]],
    ["PUT",    "/foo/{id}",       ["PUT",    "/foo/123",  "PUT",    "/foo/abc"]],
    ["DELETE", "/foo/{id}",       ["DELETE", "/foo/123",  "DELETE", "/foo/abc"]],

    ["get",    "/bar",            ["GET",    "/bar"]],
    ["get",    "/bar/{id}",       ["GET",    "/bar/123",  "GET",    "/bar/abc"]],
    ["post",   "/bar",            ["POST",   "/bar"]],
    ["put",    "/bar/{id}",       ["PUT",    "/bar/123",  "PUT",    "/bar/abc"]],
    ["delete", "/bar/{id}",       ["DELETE", "/bar/123",  "DELETE", "/bar/abc"]],

    ["GET", "/baz",               ["GET", "/baz"]],
    [["GET", "PUT"], "/baz/{id}", ["GET", "/baz/123",     "PUT", "/baz/abc"]],

    ["ANY", "/",                  ["GET", "/",            "POST", "/"]],
], [
    ["GET",   "/foobar"],
    ["PATCH", "/foo"],
    ["POST",  "/baz/123"],
]);

// Examples from https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-method-settings-method-request.html#setup-method-resources
testSet([
    ["GET", "/", ["GET", "/"]],
    ["GET", "/pets", ["GET", "/pets"]],
    ["ANY", "/pets/{petId}", ["GET", "/pets/123",  "PUT", "/pets/doge"]],
    ["GET", "/food", ["GET", "/food"]],
    ["GET", "/food/{type}/{item}", ["GET", "/food/kibble/123",  "GET", "/food/slop/123"]],
    ["ANY", "/toys", ["POST", "/toys"]],
    ["GET", "/toys/{type}/{subtype}/{item}", ["GET", "/toys/dog/chew/123",  "GET", "/toys/cat/distract/laser"]],
]);

testSet([
    ["GET", "/", ["GET", "/"]],
    ["ANY", "/{proxy+}", [
        "GET", "/pets",
        "PUT", "/pets/123",
        "GET", "/pets/doge",
        "PUT", "/food",
        "GET", "/food/kibble/123",
        "PUT", "/food/slop/123",
        "GET", "/toys",
        "PUT", "/toys/dog/chew/123",
        "GET", "/toys/cat/distract/laser",
    ]],
]);

testSet([
    ["ANY", "/{proxy+}", ["GET", "/not-parent",  "GET", "/parent",  "POST", "/parent/"]],
    ["GET", "/parent/{proxy+}", ["GET", "/parent/child",  "GET", "/parent/foo"]],
    ["ANY", "/parent/{child}/{proxy+}", ["GET", "/parent/child/grandchild",  "POST", "/parent/foo/bar"]],
]);

const errorPm = new PathMatcher();
tap.throws(() => { errorPm.set("GET", "/{proxy+}/child", ""); });
tap.throws(() => { errorPm.set("GET", "/parent/{proxy+}/{child}", ""); });
tap.throws(() => { errorPm.set("GET", "/parent/{child}/{proxy+}/{grandchild+}", ""); });

tap.throws(() => { errorPm.set("GET", "missing/starting/slash", ""); });

// Path pattern precedence
testSet([
    ["ANY", "/foo/bar/baz", ["GET", "/foo/bar/baz"]],
    ["ANY", "/foo/{f}/bar", ["GET", "/foo/bar/bar"]],
], [
    ["GET", "/foo/bar/quux"],
]);

testSet([
    ["ANY", "/foo/bar/baz", ["GET", "/foo/bar/baz"]],
    ["ANY", "/foo/{f}/baz", ["GET", "/foo/baz/baz"]],
    ["ANY", "/foo/{any+}",  ["GET", "/foo/bar/quux",  "GET", "/foo/bar"]],
], [
    ["GET", "/foo/"],
]);

// Method precedence
testSet([
    ["GET", "/foo", ["GET", "/foo"]],
    ["ANY", "/foo", ["PUT", "/foo"]],
]);

testSet([
    [["GET", "PUT"], "/foo/{id}", ["PUT",  "/foo/123"]],
    ["ANY",          "/foo/{id}", ["POST", "/foo/123"]],
]);

// Later values override earlier ones
const confusedPm = new PathMatcher();
confusedPm.set("Get", "/foo", "original value");
tap.equal(confusedPm.match("Get", "/foo"), "original value");
confusedPm.set("GET", "/foo", "new value");
tap.equal(confusedPm.match("Get", "/foo"), "new value");
