/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

'use strict';

class PathEnd {
    constructor() {
        this.methods = {};
    }

    set(methodOrMethods, value) {
        if (!Array.isArray(methodOrMethods)) {
            methodOrMethods = [methodOrMethods];
        }
        for (let method of methodOrMethods) {
            if (method.toUpperCase() === "ANY") {
                this.wildcard = value;
            } else {
                this.methods[method.toUpperCase()] = value;
            }
        }
    }

    match(method) {
        let normalizedMethod = method.toUpperCase();
        if (this.methods[normalizedMethod] != null) {
            return this.methods[normalizedMethod];
        }
        if (this.wildcard != null) {
            return this.wildcard;
        }

        return null;
    }
};

class PathPart {
    constructor() {
        this.literals = {};
    }

    addEndpoint(part, methodOrMethods, value) {
        let [p, partType] = this.getOrCreate(part);
        if (p.end == null) {
            p.end = new PathEnd();
        }
        p.end.set(methodOrMethods, value);
    }

    addPart(part) {
        let [p, partType] = this.getOrCreate(part);
        if (partType === "super-wildcard") {
            throw new Error("Super wildcards must be the last path element");
        }
        if (p.next == null) {
            p.next = new PathPart();
        }

        return p.next;
    }

    get(part, type) {
        switch (type) {
            case "super-wildcard":
                return this.superWildcard;
            case "wildcard":
                return this.wildcard;
            case "literal":
                return this.literals[part];
        }
    }

    getOrCreate(part) {
        let partType = this.parsePart(part);
        let p = this.get(part, partType);
        if (p == null) {
            if (partType === "super-wildcard") {
                this.superWildcard = {};
                p = this.superWildcard;
            } else if (partType === "wildcard") {
                this.wildcard = {};
                p = this.wildcard;
            } else {
                this.literals[part] = {};
                p = this.literals[part];
            }
        }

        return [p, partType];
    }

    match(method, parts) {
        var ret = null;

        for (let m of [this.literals[parts[0]], this.wildcard, this.superWildcard]) {
            if (m != null) {
                if ((parts.length == 1 || m === this.superWildcard) && m.end != null) {
                    // Maybe we've reached the end of the path
                    ret = m.end.match(method);
                } else if (parts.length > 1 && m.next != null) {
                    // Ok then maybe we're still on our way
                    ret = m.next.match(method, parts.slice(1))
                }
                if (ret !== null) {
                    break;
                }
            }
        }

        return ret;
    }

    parsePart(part) {
        if (part.slice(0, 1) === "{" && part.slice(-1) === "}") {
            if (part.slice(-2, -1) === "+") {
                return "super-wildcard";
            }

            return "wildcard";
        }

        return "literal";
    }
};

class PathMatcher {
    constructor() {
        this.pathStart = new PathPart();
    }

    set(methodOrMethods, pattern, value) {
        const pathParts = this.parsePath(pattern);
        var step = this.pathStart;

        while (pathParts.length > 0) {
            let pathPart = pathParts.shift();
            if (pathParts.length === 0) {
                step.addEndpoint(pathPart, methodOrMethods, value);
            } else {
                step = step.addPart(pathPart);
            }
        }
    }

    match(method, path) {
        return this.pathStart.match(method, this.parsePath(path));
    }

    parsePath(path) {
        // Sanity check: all paths must start with "/"
        if (!path || path.length < 1 || path[0] !== "/") {
            throw new Error('Path must start with "/", have ' + JSON.stringify(path));
        }

        // Remove the trailing "/", if any
        if (path.slice(-1) === "/") {
            path = path.slice(0, -1);
        }

        let parts = path.split("/").slice(1);
        if (parts.length === 0) {
            return [""]; // special case when path is just "/"
        }

        return parts;
    }
};

module.exports = PathMatcher;
